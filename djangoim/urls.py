from django.contrib import admin
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include, re_path

from .auth.views import account_profile
from .views import member_index, member_action
from .nodeApp.views import node_list, node_detail
from .editorApp.views import creat_new, node_edit, node_edit_post, module_edit_post, module_edit

from djangoim.moduleApp.views import module_list, module_detail

from djangoim.analyticsApp.views import user_history

urlpatterns = [
    # Landing page area
    re_path(r'^$', TemplateView.as_view(template_name='visitor/landing-index.html'), name='landing_index'),
    re_path(r'^about$', TemplateView.as_view(template_name='visitor/landing-about.html'), name='landing_about'),
    #re_path(r'^terms/$', TemplateView.as_view(template_name='visitor/terms.html'), name='website_terms'),
    re_path(r'^contact$', TemplateView.as_view(template_name='visitor/contact.html'), name='website_contact'),

    # Account management is done by allauth
    re_path(r'^accounts/', include('allauth.urls')),

    # Account profile and member info done locally
    re_path(r'^accounts/profile/$', account_profile, name='account_profile'),
    re_path(r'^member/$', member_index, name='user_home'),
    re_path(r'^member/action$', member_action, name='user_action'),

    #NodeApp
    re_path(r'^node/list$', node_list, name='user_node_list'),
    path('node/<int:pk>/', node_detail, name='node_detail'),

    #EditApp
     path('node/<int:pk>/edit', node_edit, name='node_edit'),
     path('node/edit', node_edit_post, name='node_edit_post'),

     path('module/<int:pk>/edit', module_edit, name='module_edit'),
     path('module/edit', module_edit_post, name='module_edit_post'),

     path('member/new/', creat_new , name='creat_new'), 
    
    #ModuleApp
    re_path(r'^module/list$', module_list, name='module_list'),
    path('module/<int:pk>/', module_detail, name='module_detail'),

    #analyticsApp
    re_path(r'^user$', user_history, name='user_history'),

    # Usual Django admin
    re_path(r'^admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
