#from .forms import NodeForm

from . models                       import Node, Edge
from djangoim.moduleApp.models      import IM
from .forms                         import UserProfileForm
from djangoim.analyticsApp.models   import UserActions

from urllib.request                 import urlopen
from markdown2                      import Markdown
import datetime
from django.db.models import Q

#ayth
from django.contrib.auth.decorators import login_required
from django.http                    import HttpResponse
from django.template                import loader

@login_required
def node_list(request):
    t = loader.get_template('nodeApp/node_list.html')

    form = UserProfileForm
    if request.method == "POST":
        form = UserProfileForm(request.POST)
        if form.is_valid():
            user_profile = form.save(user=request.user)

    nodes = get_correct_nodes(request)
    c = {'nodes':nodes, 'form':form}
    return HttpResponse(t.render(c, request), content_type='text/html')

@login_required
def node_detail(request, pk):
    t = loader.get_template("nodeApp/node.html")
    node = Node.objects.get(pk=pk)

    next_list = get_correct_edges(request, node_from=node)

    raw_txt = str(node.data)
    if(node.type_data == 'MD'):
        markdowner = Markdown()
        raw_txt = markdowner.convert(raw_txt)
        
    if(node.type_data == 'UM'): #url_to_markdown
        page = urlopen(raw_txt)
        charset = page.info().get_content_charset()
        raw_txt = page.read().decode(charset)
        markdowner = Markdown()
        raw_txt = markdowner.convert(raw_txt)
    if(node.type_data == 'UH'):#url_to_html
        page = urlopen(raw_txt)
        charset = page.info().get_content_charset()
        raw_txt = page.read().decode(charset)
    if(node.type_data == 'UF'):#url_to_html_form
        page = urlopen(raw_txt)
        charset = page.info().get_content_charset()
        raw_txt = page.read().decode(charset)


    next_node = None

    if(len(next_list) ==0):
        next_list = 'None'
    else:
        next_node = next_list[0].node_to

    # logging
    if request.method == "POST":
        res = request.POST.dict()
        del res['csrfmiddlewaretoken']
        UserActions(user=request.user,im=request.user.profile.correct_module, node=node, data=str(res)).save()
    else:
        UserActions(user=request.user,im=request.user.profile.correct_module, node=node).save()
        
    c = {'node':node,
         'page':raw_txt, 
         "next_list":next_list, 
         "next_node":next_node
         }
    return HttpResponse(t.render(c, request), content_type='text/html')

def get_correct_nodes(request):
    nodes =Node.objects.order_by('created_date').filter(complexity_data='B')
    if request.user.profile.complexity_data == "A":#Q(creator=owner) | Q(moderated=False)
        nodes =Node.objects.order_by('created_date')
    if request.user.profile.complexity_data == "N":#Q(creator=owner) | Q(moderated=False)
        nodes =Node.objects.order_by('created_date').filter(Q(complexity_data='B') | Q(complexity_data='N'))

    return nodes

def get_correct_edges(request, node_from=None, im=None):
    edges=Edge.objects.order_by("-weight")
    if node_from != None:
        edges=edges.filter(node_from=node_from)
    if im != None: # IM
        edges=edges.filter(supervisor_im=im)
    edges_list = []
    nodes = get_correct_nodes(request)
    for edge in edges:
        if edge.node_from in nodes and edge.node_to in nodes:
            edges_list.append(edge)
    return edges_list

def get_module_nodes(request,im, node_from=None):
    edges = get_correct_edges(request,im=im,node_from=node_from,)
    edges_set = set()
    for edge in edges:
        edges_set.add(edge.node_from)
        edges_set.add(edge.node_to)
    return edges_set