from django.db                  import models
from django.utils               import timezone
from djangoim.moduleApp.models  import IM

class Node(models.Model):

    # Fields
    author = models.ForeignKey('djangoim_auth.User', on_delete=models.CASCADE, related_name='+')
    title = models.CharField(max_length=200)

    
    TYPE_CHOICES = (
        ('UM', 'url_to_markdown'),
        ('UH', 'url_to_html'),
        ('UF', 'url_to_html_form'),
        ('MD', 'markdown'), 
    )
    type_data = models.CharField(max_length=2,
                                 choices=TYPE_CHOICES,
                                 default='MD',
                                 verbose_name="Тип"
                                 )

    COMPLEXITY_CHOICES = (
        ('B', 'Base'),
        ('N', 'Normal'),
        ('A', 'Additional'),
    )
    complexity_data = models.CharField(max_length=1,
                                 choices=COMPLEXITY_CHOICES,
                                 default='B',
                                 verbose_name="Сложность"
                                 )

    data = models.TextField() # content
    meta_data = models.TextField(blank=True, null=True, help_text="Enter node description") #TODO: add tages system
    created_date = models.DateTimeField(
            default=timezone.now)
            
    # Methods
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('node_detail', args=[str(self.id)])

class Edge(models.Model):

    # Fields
    weight = models.FloatField(default=1)
    data = models.TextField(blank=True, null=True)

    node_from = models.ForeignKey(Node, on_delete=models.CASCADE,related_name='edge_to')
    node_to = models.ForeignKey(Node, on_delete=models.CASCADE,related_name='edge_from')
    
    supervisor_im = models.ForeignKey(IM, on_delete=models.CASCADE,blank=True,  null=True, related_name='supervisor_im')
    # Methods
    def __str__(self):
        return "{} -> {}".format(self.node_from,self.node_to)
