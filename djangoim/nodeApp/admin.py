from django.contrib import admin

# Register your models here
from .models        import Node, Edge

class NodeAdmin(admin.ModelAdmin):
    search_fields   = ('title','data','author')
    ordering        = ('created_date',)
    list_display    = ('title', 'author','complexity_data','created_date')
    list_filter     = ('complexity_data',)

admin.site.register(Node, NodeAdmin)
admin.site.register(Edge)
