import datetime
#ayth
from django.contrib.auth.decorators import login_required
from django.http                    import HttpResponse
from django.template                import loader

from djangoim.analyticsApp.models   import UserActions

@login_required
def user_history(request):
    t = loader.get_template('analyticsApp/user-history.html')
    
    actions = UserActions.objects.filter(user=request.user)
    c = {'actions':actions}
    return HttpResponse(t.render(c, request), content_type='text/html')