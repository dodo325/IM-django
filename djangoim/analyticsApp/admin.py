from django.contrib import admin
from .models        import UserActions

class UserActionsAdmin(admin.ModelAdmin):
    list_display  = ('user', 'im', 'node', 'created_date')
    ordering      = ('created_date',)
    search_fields = ('data',)

admin.site.register(UserActions, UserActionsAdmin)