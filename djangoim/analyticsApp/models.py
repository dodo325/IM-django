from django.db import models
from django.utils import timezone

from djangoim.moduleApp.models import IM
from djangoim.nodeApp.models import Node, Edge

class UserActions(models.Model):
    user = models.ForeignKey('djangoim_auth.User', on_delete=models.CASCADE)
    im = models.ForeignKey(IM, blank=True, null=True, on_delete=models.CASCADE,related_name='im')
    node = models.ForeignKey(Node, blank=True, null=True, on_delete=models.CASCADE,related_name='node')
    data = models.TextField(blank=True, null=True, help_text="JSON")
    # description = models.TextField(blank=True, null=True, help_text="Description")
    created_date = models.DateTimeField(
            default=timezone.now)

    # Methods
    def __str__(self):
        return "{} <-> {}".format(self.user,self.node)
    