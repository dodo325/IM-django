from django.contrib import admin

from .models        import IM,EdgeIM

def make_published(modeladmin, request, queryset):
    queryset.update(published=True)
make_published.short_description = "Опубликовать"

def dispublished(modeladmin, request, queryset):
    queryset.update(published=False)
dispublished.short_description = "Снять с публикации"

class IMAdmin(admin.ModelAdmin):
    list_filter     = ('published',)
    search_fields   = ('title','description')
    ordering        = ('created_date',)
    list_display    = ('title', 'author','created_date','published')
    actions         = [make_published, dispublished]

admin.site.register(IM, IMAdmin)
admin.site.register(EdgeIM)
