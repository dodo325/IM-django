#ayth
from django.contrib.auth.decorators import login_required
from django.http                    import HttpResponse
from django.template                import loader

from markdown2                      import Markdown
# App
#   models
from djangoim.auth.models           import UserProfile
from djangoim.moduleApp.models      import IM, EdgeIM
from djangoim.nodeApp.models        import Node, Edge

#   
from djangoim.nodeApp.forms         import UserProfileForm
from djangoim.nodeApp.views         import get_correct_edges, get_correct_nodes, get_module_nodes

@login_required
def module_list(request):
    t = loader.get_template('moduleApp/module_list.html')
     
    edge_list = []    # valid(published) edges:
    for edge in EdgeIM.objects.all(): 
        if( (edge.im_from.published==True) and (edge.im_to.published==True) ):
            edge_list.append(edge)

    c = {'im_edg':edge_list, 
         'im_arr':IM.objects.filter(published=True).order_by('created_date')}
    return HttpResponse(t.render(c, request), content_type='text/html')

@login_required
def module_detail(request, pk):
    t = loader.get_template('moduleApp/module_detail.html')
    im = IM.objects.get(pk=pk)

    request.user.profile.correct_module = im
    request.user.profile.save()

    form = UserProfileForm
    if request.method == "POST":
        form = UserProfileForm(request.POST)
        if form.is_valid():
            user_profile = form.save(user=request.user)

    nodes = get_module_nodes(request, im)
    
    markdowner = Markdown()
    im_description = markdowner.convert(im.description)

    c = {'im':im, 'nodes':nodes, 'form':form,'im_description':im_description}
    return HttpResponse(t.render(c, request), content_type='text/html')