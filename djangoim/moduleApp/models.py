from django.db      import models
from django.utils   import timezone
from django.urls    import reverse

class IM(models.Model):
    # Fields
    author      = models.ForeignKey('djangoim_auth.User', on_delete=models.CASCADE)
    title       = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True, help_text="Enter IM description")    #TODO: add tages
    
    created_date = models.DateTimeField(default=timezone.now)
    published    = models.BooleanField(default='False')

    def get_absolute_url(self):
        return reverse('module_detail', args=[str(self.id)])

    # Meta
    class Meta:
        verbose_name_plural = "list IM"

    # Methods
    def __str__(self):
        return self.title

class EdgeIM(models.Model):
    # Fields
    weight  = models.FloatField(default=1)
    data    = models.TextField(blank=True, null=True)

    im_from = models.ForeignKey(IM, on_delete=models.CASCADE,related_name='im_edge_to')
    im_to   = models.ForeignKey(IM, on_delete=models.CASCADE,related_name='im_edge_from')
    
    # Methods
    def __str__(self):
        return "{} -> {}".format(self.im_from,self.im_to)
    class Meta:
        verbose_name_plural = "Edges"