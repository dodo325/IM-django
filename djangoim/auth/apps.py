from django.apps import AppConfig


class djangoIMAuthAppConfig(AppConfig):
    label = "djangoim_auth"
    name = "djangoim.auth"
    verbose_name = "djangoIM Auth"
