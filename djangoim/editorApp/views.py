#ayth
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf   import csrf_protect
from django.http                    import HttpResponse
from django.template                import loader

from djangoim.moduleApp.models      import IM, EdgeIM
from djangoim.nodeApp.models        import Node, Edge
from djangoim.analyticsApp.models   import UserActions

# NEW
@login_required
def creat_new(request):
    t = loader.get_template('editorApp/new.html')
    created_im     =   IM.objects.filter(author=request.user) 
    created_node   = Node.objects.filter(author=request.user)
    num_creat_im   = len(created_im)
    num_creat_node = len(created_node)

    c = {'num_creat_im':num_creat_im,
         'num_creat_node':num_creat_node,
         'created_im':created_im,
         'created_node':created_node
        }
    return HttpResponse(t.render(c, request), content_type='text/html')

# IM 
@login_required
@csrf_protect
def module_edit_post(request):
    if(request.method == "POST"):
        res = request.POST.dict()

        if(res['event'] == 'new_im'):
            im = IM.objects.create(
                author=request.user,
                title='Название',
            )
            im.save()
            return HttpResponse(im.pk)
        
        if(res['im_pk'] == ''):
            return HttpResponse('404')
        
        im = IM.objects.get(pk=res['im_pk'])

        if(res['event'] == 'save'):
            im.description = res['description'] 
            im.title = res['title']
            #TODO: if form.is_valid(): 
            im.save()

        if(res['event'] == 'delete'):
            print('delete im')
            im.delete()

        return HttpResponse('OK')
    else:
        return HttpResponse('404')

@login_required
def  module_edit(request,pk):
    im = IM.objects.get(pk=pk)

    t = loader.get_template('editorApp/im_edit.html')
    if(str(request.user) != str(im.author)):
        return HttpResponse('404')
    
    c = {'im':im,
        }
        
    return HttpResponse(t.render(c, request), content_type='text/html')

# Node
@login_required
@csrf_protect
def node_edit_post(request):
    if(request.method == "POST"):
        res = request.POST.dict()

        if(res['event'] == 'new_node'):
            node = Node.objects.create(
                author=request.user,
                title='Название',
                data='Content',
            )
            node.save()
            return HttpResponse(node.pk)

        if(res['node_pk'] == ''):
            return HttpResponse('404')

        node = Node.objects.get(pk=res['node_pk'])
        if(res['event'] == 'save'):
            node.data = res['data'] 
            node.title = res['title']
            node.complexity_data = res['complexity_data']
            #TODO: if form.is_valid(): 
            node.save()

        if(res['event'] == 'delete'):
            node.delete()

        return HttpResponse('OK')
    else:
        return HttpResponse('404')

@login_required
def node_edit(request,pk):
    node = Node.objects.get(pk=pk)

    t = loader.get_template('editorApp/node_edit_md.html')
    if(str(request.user) != str(node.author)):
        return HttpResponse('404')
    
    c = {'node':node,
         'type_data':node.get_type_data_display(),
         'complexity_data':node.get_complexity_data_display()
        }
        
    return HttpResponse(t.render(c, request), content_type='text/html')
