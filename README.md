# IM-django

## Setup:
```bush
python manage.py makemigrations analyticsApp djangoim_auth moduleApp nodeApp editorApp
python manage.py migrate
python manage.py createsuperuser
python manage.py set_auth_provider google [id] [key]
python manage.py set_auth_provider facebook [id] [key]
python manage.py set_auth_provider vk [id] [key]
python manage.py set_auth_provider github [id] [key]
```