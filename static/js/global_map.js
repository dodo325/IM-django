var cy = window.cy = cytoscape({
    container: document.getElementById('cy'),
  
    boxSelectionEnabled: false,
    autounselectify: true,
  
    layout: {
      name: 'dagre'
    },
  
    style: [
      {
        selector: 'node',
        style: {
          'content': 'data(id)',
          'text-opacity': 0.5,
          'text-valign': 'center',
          'text-halign': 'right',
          'background-color': '#11479e'
        }
      },
      
      {
        selector: '#1',
        style: {
  
          'background-color': '#9e1134'
        }
      },
  
      {
        selector: 'edge',
        style: {
          'curve-style': 'bezier',
          'width': 4,
          'target-arrow-shape': 'triangle',
          'line-color': '#9dbaea',
          'target-arrow-color': '#9dbaea'
        }
      }
    ],
  
    elements: {
      nodes: [
        { data: { id: '1' } },
        { data: { id: '2' } },
        { data: { id: '3' } },
      ], 
      edges: [
        { data: { source: '1', target: '2' } },
        { data: { source: '2', target: '3' } },
      ]
    },
  });
  cy.on('tap', 'node', function(evt){
    var node = evt.target;
    alert(elr);
  });
  